interp2dpp
==========
*An easy to use C++ wrapper for GSL interpolation routines*

interp2dpp is an easy to use C++ library for interpolating two-dimensional data.
It is really a (not-so) glorified C++ wrapper for the GSL interpolation
methods `gsl_interp_akima` and `gsl_interp_linear` with some built-in data sorting
utilities.

## INSTALLATION ##

Make sure the [GNU Scientific Library][] and header files are installed in standard locations.
For detailed instructions regarding installation, read the `INSTALL` file.

Briefly, doing the following steps will install the library and headers in the
standard locations (/usr/local/lib(64)/ and /usr/local/include/ respectively):

    ./configure
    make -j<n>
    make install

(where <n\> is an integer specifying the number of parallel make threads to run
during compilation, e.g. make -j4 on a quad-core processor)

To configure the installation location, e.g. to /usr instead of /usr/local,
the `--prefix` argument has to be passed to `./configure` appropriately. Eg.,  
`./configure --prefix=/usr`

The usual make and make install as above will then install the library and
header file to /usr/lib(64)/ and /usr/include/ respectively.

## USAGE ##

The `example.cc` file installed with the library (in system docdir,
by default /usr/share/doc/interp2dpp) which shows a typical usage as well as how to
link against the library.

[GNU Scientific Library]: http://www.gnu.org/software/gsl/