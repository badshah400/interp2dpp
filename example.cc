// example.cc
//
// Copyright (C) 2014 - Atri Bhattacharya
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// To link this file against interp2dpp, one does
// g++ -Wall -O3 `pkg-config --cflags --libs interp2dpp` example.cc -o exmpl.x

#include <iostream>
#include "interp2d.hpp"
#include <vector>

using namespace std;

inline double sqr(const double x)
{
	return x * x;
};

int main()
{
	vvdouble discr_data;

//	Generate a set of discrete x and y values, e.g., x^2 vs x
	for (unsigned int ipts = 0; ipts < 10; ++ipts)
	{
		vector<double> tmp(2);
		tmp.at(0) = ipts + 1;
		tmp.at(1) = 1.0 / (sqr(tmp.at(0)));
		discr_data.push_back(tmp);
 	}

	cout << "Input data" << endl
		 << "---------------------" << endl;
	for (unsigned int i = 0; i < discr_data.size(); i += 1)
	{
		cout << discr_data.at(i).at(0) << '\t'
			 << discr_data.at(i).at(1)
		     << endl;
	}
	cout << "---------------------" << endl;


//	Construct the interp_data object
//	Other than the constructor shown here, a (useless) default constructor, copy
//	constructor and the assignment (=) operation are implemented.
//	vvdouble::at(0) is always used as the default x axis.
//	When nothing else is specified, the vvdouble::at(1) as the default y axis.
//	Pass a valid column number, e.g., interp_data (discr_data, ncol) to use
//	vvdouble::at(ncol) as the y axis, instead of column 1.
//	Pass a third integer to specify which column to use as the x-axis, such as
//	interp_data id(discr_data, 2, 3) would set up the 3rd column of discr_data
//	as the x-axis with the 2nd column as the y-axis.
//	Data is automatically sorted by the x-axis. Interpolation will done for
//	y-axis vs the x-axis.

//	Default usage: column 1 vs column 0
	interp_data id(discr_data);

//	Interpolation of column 0 vs column 1; automatic sorting done for unsorted
//	x-axis
	interp_data id2(discr_data, 0, 1);
	
//	Is the interp_data object sorted by the x-axis (remember, interpolation
//	would otherwise fail)
//	In practice, since the interp_data constructor already sorts incoming data,
//	one would probably never need this
	if (!id2.data_is_sorted())
		id2.sort_data();

//	Now for the two interpolation methods...
//	In goes the x and the f(x) is returned as a double
//	The akima method yields significantly better results, and is strongly
//	recommended over the linear interpolation. If the data known at sufficiently
//	small step sizes, the linear method may be used (it is only slightly faster)
	cout << "1/4^2 is " << id.interp_akima(4)  << endl;
	cout << "4.67^{-2} is " << id.interp_akima(4.67)  << endl;
	cout << "2.50^{-2} is " << id.interp_linear(2.50) << endl;
	cout << "1/Sqrt(1/4.61) is " << 1.0 / id2.interp_akima (1.0/4.61) << endl;

//	The interp_data object can also be created using a constructor of format
//	interp_data(ndata, xdata, ydata)
//	where xdata and ydata are iterators to the head of some contiguous
//	data structure with ndata elements each. For example,
	std::vector<double> xvec, yvec;
	for (unsigned int i = 0; i < discr_data.size(); i += 1)	
	{
		xvec.push_back(discr_data.at(i).at(0));
		yvec.push_back(discr_data.at(i).at(1));
	}
	interp_data id3(xvec.size(), xvec.begin(), yvec.begin());
	cout << "1/5^2 is " << id3.interp_akima(5) << endl;

//	Or, for that matter, with C arrays:
	const unsigned int nsz = discr_data.size();
	double xarr[nsz], yarr[nsz];
	for (unsigned int i = 0; i < nsz; ++i)
	{
		xarr[i] = discr_data.at(i).at(0);
		yarr[i] = discr_data.at(i).at(1);
	}
	interp_data id4(nsz, xarr, yarr);
	cout << "1/3.25^2 is " << id4.interp_akima(3.25) << endl;
	
//	...and so on. One could use boost::array also with this form
//	of the constructor, for instance.
	
	
	return 0;
}