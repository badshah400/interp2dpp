/* vim: set cin ts=4 sw=4 tw=80: */
/*
 * interp2d.hpp
 * Copyright (C) 2014 Atri Bhattacharya <badshah400@gmail.com>
 *
 * interp2dpp is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * interp2dpp is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _INTERP2D_HPP_
#define _INTERP2D_HPP_

#include <vector>

/* undef vvdouble if it has already be defined, e.g., when using together
 * with tabdatrw library which also defines it exactly identically.
 */
#ifdef vvdouble
#undef vvdouble
#endif
typedef std::vector<std::vector<double> > vvdouble;

template <class ForwardIterator>
bool is_sorted (ForwardIterator first, ForwardIterator last)
{
	if(first == last) return true;
	ForwardIterator next = first;
	while(++next != last)
	{
		if(*next < *first)
			return false;
		++first;
	}
	return true;
}

class interp_data
{
private:
	double ** idat;
	static const unsigned int dim;
	unsigned int dat;
	unsigned int no_data_flag;

public:
	interp_data() { idat = NULL; };
	interp_data(const interp_data &);
	interp_data(const vvdouble &,
	            const unsigned int = 1,
	            const unsigned int = 0);
	template <typename T>
	interp_data(const unsigned int nsize, const T t1, const T t2)
	{
		idat = new double * [dim];
		dat  = nsize;
		for (unsigned int i = 0; i < dim; ++i)
		{
			idat[i] = new double [dat];
		}
		for (unsigned int i = 0; i < dat; ++i)
		{
			idat[0][i] = *(t1 + i);
			idat[1][i] = *(t2 + i);
		}
		if(!this->data_is_sorted())
			this->sort_data();

	}
				
	~interp_data();

	const interp_data & operator=(const interp_data &);

	void sort_data();

	bool data_is_sorted();

	inline double xmin() const
	{
		return idat[0][0];
	};

	inline double xmax() const
	{
		return idat[0][dat-1];
	};

	double interp_akima(const double) const;
	double interp_linear(const double) const;
};

#endif // _INTERP2D_HPP_