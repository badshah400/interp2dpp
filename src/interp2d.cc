/* vim: set cin ts=4 sw=4 tw=80: */
/*
 * interp2d.cc
 * Copyright (C) 2014 Atri Bhattacharya <badshah400@gmail.com>
 *
 * interp2dpp is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * interp2dpp is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <vector>
#include <algorithm>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

#include "interp2d.hpp"

using namespace std;

struct veccomp
{
	bool operator () (vector<double> x, vector<double> y)
	{
		return(x.at(0) < y.at(0));
	}
} vcomp;


const unsigned int interp_data::dim = 2;

interp_data::interp_data(const vvdouble & v,
                         const unsigned int c2,
						 const unsigned int c1)
{
	dat = v.size();
	idat = new double * [dim];
	for (unsigned int i = 0; i < dim; i += 1)
	{
		idat[i] = new double [dat];
	}
	for (unsigned int i = 0; i < dat; i += 1)
	{
		idat[0][i] = v.at(i).at(c1);
		idat[1][i] = v.at(i).at(c2);
	}
	if(!this->data_is_sorted())
		this->sort_data();

}

interp_data::interp_data(const interp_data & xi)
{
	dat = xi.dat;
	idat = new double * [dim];
	for (unsigned int i = 0; i < dim; i += 1)
	{
		idat[i] = new double [dat];
	}
	for (unsigned int i = 0; i < dim; i += 1)
	{
		for (unsigned int j = 0; j < dat; j += 1)
		{
			idat[i][j] = xi.idat[i][j];
		}
	}

}


interp_data::~interp_data()
{
//	Only delete if idat points to valid memory
	if (idat)
	{
		for (unsigned int i = 0; i < dim; i += 1)
		{
			delete [] idat[i];
		}
		delete [] idat;
	}
}

const interp_data & interp_data::operator=(const interp_data & xi)
{
//	Only delete if idat points to valid memory
	if (idat)
	{
		for (unsigned int i = 0; i < dim; i += 1)
		{
			delete [] idat[i];
		}
		delete [] idat;
	}
	dat = xi.dat;
	idat = new double * [dim];
	for (unsigned int i = 0; i < dim; i += 1)
	{
		idat[i] = new double [dat];
	}
	for (unsigned int i = 0; i < dim; i += 1)
	{
		for (unsigned int j = 0; j < dat; j += 1)
		{
			idat[i][j] = xi.idat[i][j];
		}
	}

	return *this;

}


double interp_data::interp_akima(const double x0) const
{
	double y0;
	{
		gsl_interp_accel *acc = gsl_interp_accel_alloc ();
		gsl_spline    *spline = gsl_spline_alloc (gsl_interp_akima, dat);
		gsl_spline_init (spline, idat[0], idat[1], dat);

		y0 = gsl_spline_eval (spline, x0, acc);
		gsl_spline_free (spline);
		gsl_interp_accel_free (acc);
	}

	return y0;

}

double interp_data::interp_linear(const double x0) const
{
	double y0;
	{
		gsl_interp_accel *acc = gsl_interp_accel_alloc ();
		gsl_spline    *spline = gsl_spline_alloc (gsl_interp_linear, dat);
		gsl_spline_init (spline, idat[0], idat[1], dat);

		y0 = gsl_spline_eval (spline, x0, acc);
		gsl_spline_free (spline);
		gsl_interp_accel_free (acc);
	}

	return y0;

}

bool interp_data::data_is_sorted()
{
	return is_sorted(idat[0], idat[0] + dat);
}

void interp_data::sort_data()
{
	vvdouble tosort;
	for (unsigned int i = 0; i < dat; i += 1)
	{
		vector<double> tmp;
		tmp.push_back(idat[0][i]);
		tmp.push_back(idat[1][i]);
		tosort.push_back(tmp);
	}
	std::sort(tosort.begin(), tosort.end(), vcomp);
	for (unsigned int i = 0; i < dat; ++i)
	{
		for (unsigned int j = 0; j < dim; ++j)
			idat[j][i] = tosort.at(i).at(j);
	}
	return;
}